# Lab 3

## Software Needed

- python
- flask
- CORS browser extension

## Installation Instructions

 - `python` should be installled on your machines. To verify type `python --version` on your command prompts. In case it hasn't been installed, please download the installer from [here](https://www.python.org/downloads/windows/) and follow the on-screen steps to complete installation

 - Once `python` is installed, pip3 is automatically installed too. It should be added to your PATH environment variable, allowing you to call pip3 from the command prompt.

 - Please run `pip3 install flask` on your command prompt to install Flask on your machine. If you get a "pip3 not found" error while running this command, please click on the Python executable you downloaded in Step 1, click on Modify Installation and there would be a checkbox that says "Add python to PATH". Please click on this checkbox and continue the installation as done earlier. pip3 should now be available when you open a new command prompt.

 - Please install the  [CORS browser extension](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en), in case you haven't installed it already. After installation, please switch it on to enable GET/POST requests to be made to the APIs in `app.py`
 
 - Then run `python app.py` after downloading it from the gitlab repo, to start the Flask server on your laptop.
