data = {"Alice": ["24", "33", "30", "66", "37"], "Bob": ["16", "29", "53", "61", "97"], "Sanya": ["64", "65", "61", "46", "50"], "Colin": ["81", "12", "48", "21", "99"], "Kiara": ["53", "97", "79", "41", "66"], "Margaret": ["57", "46", "94", "99", "19"], "Emily": ["3", "97", "52", "59", "34"], "Mohan": ["98", "81", "54", "24", "18"], "Joe": ["39", "20", "39", "44", "21"], "Mia": ["16", "79", "57", "42", "55"]}

function getHighestMarks() {
	max = 0;
	maxArg = "";

	for (obj in data){
		sum = 0;
		//Sum all the marks for student "obj"
		for (i in data[obj])
			sum += data[obj];

		// Update max value if sum>max
		if (sum>max){
			max = sum;
			maxArg = obj; //Contains the person with maximum marks
		}
	}
	return maxArg;
}

function getSubject2Toppers() {

	// Create items array with marks for only the second subject
	var items = Object.keys(data).map(function(key) {
	  return [key, data[key][1]];
	});

	// Sort the array based on the second element
	items.sort(function(first, second) {
	  return first[1] - second[1];
	});

	return items;
}

console.log(getHighestMarks())
console.log(getSubject2Toppers())