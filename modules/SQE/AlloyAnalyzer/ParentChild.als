// An object in our model that represents a Human
sig Human { parent: lone Parent }

// An object that represents a Parent: a subclass of Human
// A Parent has children that belong to the set of Humans
sig Parent extends Human { children: set Human }

// An object that represents a Child: a subclass of Human
sig Child extends Human { }

// An instance of the Parent object is the parent of its children
fact { all p: Parent, c: p.children | c.parent = p }

// All humans are either children or parents
fact { Child + Parent = Human }

// An object representing an 'Ancestor' who started humanity
// This Ancestor has no Parent
one sig Ancestor extends Parent { } { no parent }

// The children of the Ancestor were humans
fact { Human in Ancestor.*children}

run {} for 3

// An acyclic assertion: that is, no Parent belongs to their own Children
assert acyclic { no p: Parent | p in p.^children}

// Now check it for a scope of 5
check acyclic for 5

// Humanity has one and only one Ancestor, a Human who had no Parents
assert oneAncestor { one p: Parent | no p.parent }

// Now check it for a scope of 5
check oneAncestor for 5

/* Every Child has only one Parent 
   (Note here we are considering mother and father to comprise 
   a single Parent instance so as to not overcomplicate the model) */
assert onlyOneParent { all h: Human | lone p: Parent | h in p.children }

// Now check it for a scope of 5
check onlyOneParent for 5

/* An assertion with a counterexample
  Write an assertion here that claims that all non-Ancestor
  Humans have the same Parent */


/* Now that you know this assertion is incorrect, 
  write a fix for the assertion below */