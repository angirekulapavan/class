import java.io.*;
import java.util.Properties;

// import java.util.Vector;
// import java.io.*;

public class drive {

	public static void main(String[] args) {

		Properties prop = new Properties();
		String filename = "config";
		InputStream is = null;
		try {
			is = new FileInputStream(filename);
		} catch (FileNotFoundException ex) {
			// TODO: handle exception
		}
		try {
			prop.load(is);
		} catch (IOException ex) {
			// TODO: handle exception
		}
		try {
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LocalDatabase.initDB();

		int numLanes = Integer.parseInt(prop.getProperty("number_of_lanes"));
		int maxPatronsPerParty=Integer.parseInt(prop.getProperty("max_patrons_per_party"));


		// Alley a = new Alley( numLanes );
		// ControlDesk controlDesk = a.getControlDesk();

		ControlDesk controlDesk = new ControlDesk(numLanes);

		ControlDeskView cdv = new ControlDeskView( controlDesk, maxPatronsPerParty);
		controlDesk.subscribe( cdv );

	}
}
