import java.sql.*;
import java.util.*;

public class LocalDatabase {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static String DB_URL = "jdbc:mysql://localhost/";

    // Database credentials
    static final String USER = "naruto";
    static final String PASS = "9tails";

    public static Vector<HashMap<String, String> > getRecord() {
        Connection conn = null;
        Statement stmt = null;

        Vector<HashMap<String, String> > results = new Vector<HashMap<String, String> >();
        HashMap<String, String> result = new HashMap<String, String>();
        try {
            // STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT id, nick, date, score FROM Bowlers";
            ResultSet rs = stmt.executeQuery(sql);

            // STEP 5: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                int id = rs.getInt("id");
                String nick = rs.getString("nick");
                String date = rs.getString("date");
                String score = rs.getString("score");

                result.put("nick", nick);
                result.put("date", date);
                result.put("score", score);
                results.add(result);


                // Display values
                System.out.print("ID: " + id);
                System.out.print(", Nick Name: " + nick);
                System.out.print(", Date: " + date);
                System.out.println(", Score: " + score);
            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return results;
    }

    public static void writeRecord(String query) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(JDBC_DRIVER);

            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");

            System.out.println("Inserting records into the table...");
            stmt = conn.createStatement();

            stmt.executeUpdate(query);

            System.out.println("Inserted records into the table...");
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    conn.close();
            } catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }        
    }

    public static void initDB() {
        Connection conn = null;
        Statement stmt = null;
        String dbName = "SCORES";

        boolean exists = false;
        try {
            Class.forName(JDBC_DRIVER);

            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            
            ResultSet result = conn.getMetaData().getCatalogs();

            while(result.next()) {
                String tempName = result.getString(1);
                if (tempName.equals(dbName)) {
                    exists = true;
                    break;
                }
            }
            result.close();
            // conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (exists) {
            String tempURL = DB_URL;
            DB_URL = tempURL + dbName;
        } else {
            try {
                System.out.println("Creating database...");
                stmt = conn.createStatement();

                String sql = "CREATE DATABASE SCORES";
                stmt.executeUpdate(sql);
                System.out.println("Database created successfully...");
                stmt.close();
                conn.close();
                String tempURL = DB_URL;
                DB_URL = tempURL + dbName;

                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                stmt = conn.createStatement();
                System.out.println("Creating table in given database...");
                sql = "CREATE TABLE Bowlers " +
                      "(id INTEGER not NULL AUTO_INCREMENT, " +
                      " nick VARCHAR(255), " +
                      " date VARCHAR(255), " +
                      " score VARCHAR(255), " +
                      " PRIMARY KEY ( id ))";
                stmt.executeUpdate(sql);
                System.out.println("Created table in given database...");
            } catch (SQLException se) {
                // Handle errors for JDBC
                se.printStackTrace();
            } catch (Exception e) {
                // Handle errors for Class.forName
                e.printStackTrace();
            }
        }

        try {
            if (stmt != null)
                stmt.close();
        } catch (SQLException se2) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
        
    }
}