import java.util.Vector;
import java.io.*;

public class drive {

	private static ControlDesk controlDesk;
	private static ControlDeskView cdv;
	public static void main(String[] args) {

		int numLanes = 3;
		int maxPatronsPerParty=5;

		controlDesk = ControlDesk.getInstance(numLanes);

		cdv = new ControlDeskView( controlDesk, maxPatronsPerParty);
		controlDesk.subscribe( cdv );

	}
}
