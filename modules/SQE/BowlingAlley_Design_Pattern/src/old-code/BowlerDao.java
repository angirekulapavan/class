import java.util.List;
import java.util.*;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface BowlerDao {
   public Vector getBowlers() throws IOException, FileNotFoundException;
   public Bowler getBowlerInfo(String nickName) throws IOException, FileNotFoundException;
   public void putBowlerInfo(
		String nickName,
		String fullName,
		String email) throws IOException, FileNotFoundException;
}