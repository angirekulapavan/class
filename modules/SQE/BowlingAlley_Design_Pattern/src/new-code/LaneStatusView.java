/**
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class LaneStatusView implements ActionListener, LaneObserver, PinsetterObserver {

	private JPanel jp;

	private JLabel curBowler, foul, pinsDown;
	private JButton viewExpandedLane, maintenance;

	private ExpandedLaneView elv;
	private Lane lane;
	int laneNum;

	boolean expandedLaneShowing;

	public LaneStatusView(Lane lane, int laneNum ) {

		this.lane = lane;
		this.laneNum = laneNum;

		expandedLaneShowing = false;
		elv = new ExpandedLaneView(lane, laneNum);

		jp = new JPanel();
		jp.setLayout(new FlowLayout());
		JLabel cLabel = new JLabel( "Now Bowling: " );
		curBowler = new JLabel( "(no one)" );
		JLabel fLabel = new JLabel( "Foul: " );
		foul = new JLabel( " " );
		JLabel pdLabel = new JLabel( "Pins Down: " );
		pinsDown = new JLabel( "0" );

		// Button Panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());

		Insets buttonMargin = new Insets(4, 4, 4, 4);

		viewExpandedLane = new JButton("Expand");
		JPanel viewExpandedLanePanel = new JPanel();
		viewExpandedLanePanel.setLayout(new FlowLayout());
		viewExpandedLane.addActionListener(this);
		viewExpandedLanePanel.add(viewExpandedLane);

		maintenance = new JButton("Resume");
		maintenance.setBackground( Color.GREEN );
		JPanel maintenancePanel = new JPanel();
		maintenancePanel.setLayout(new FlowLayout());
		maintenance.addActionListener(this);
		maintenancePanel.add(maintenance);

		viewExpandedLane.setEnabled( false );

		buttonPanel.add(viewExpandedLanePanel);
		buttonPanel.add(maintenancePanel);

		jp.add( cLabel );
		jp.add( curBowler );
//		jp.add( fLabel );
//		jp.add( foul );
		jp.add( pdLabel );
		jp.add( pinsDown );
		
		jp.add(buttonPanel);

	}

	public JPanel showLane() {
		return jp;
	}

	public void expandedLaneView(){
		if ( expandedLaneShowing == false ) {
			elv.show();
			expandedLaneShowing = true;
		} else if ( expandedLaneShowing == true ) {
			elv.hide();
			expandedLaneShowing = false;
		}
	}

	public void actionPerformed( ActionEvent e ) {
		if (e.getSource().equals(viewExpandedLane) && lane.isPartyAssigned()) {
			expandedLaneView();
		}
		if (e.getSource().equals(maintenance)) {
			if ( lane.isPartyAssigned() ) {
				lane.unPauseGame();
				maintenance.setBackground( Color.GREEN );
			}
		}
	}

	public void receiveLaneEvent(LaneEvent le) {
		curBowler.setText( ( (Bowler)le.getBowler()).getNickName() );
		if ( le.isMechanicalProblem() ) {
			maintenance.setBackground( Color.RED );
		}	
		if ( lane.isPartyAssigned() == false ) {
			viewExpandedLane.setEnabled( false );
		} else {
			viewExpandedLane.setEnabled( true );
		}
	}

	public void enableExpandedLane() {
		viewExpandedLane.setEnabled(true);
	}

	public void receivePinsetterEvent(PinsetterEvent pe) {
		pinsDown.setText( ( new Integer(pe.totalPinsDown()) ).toString() );
//		foul.setText( ( new Boolean(pe.isFoulCommited()) ).toString() );
		
	}

}
