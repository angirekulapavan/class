import java.util.Vector;
import java.io.*;

public class drive {

	private static ControlDesk controlDesk;
	private static ControlDeskView cdv;
	private static StartMenu startMenu;
	public static void main(String[] args) {

		StartMenu startMenu = new StartMenu();
		int numLanes = startMenu.getNumLanes();
		int maxPatronsPerParty = startMenu.getMaxPPP();
		String difficulty = startMenu.getDifficulty();

		System.out.println(numLanes);
		System.out.println(maxPatronsPerParty);
		System.out.println(difficulty);

		controlDesk = ControlDesk.getInstance(numLanes, difficulty);

		cdv = new ControlDeskView( controlDesk, maxPatronsPerParty);
		controlDesk.subscribe( cdv );

	}
}
