/*
 *  constructs a prototype Lane View
 *
 */

import java.awt.*;
import java.awt.event.*;
import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.DataLine;
import javax.swing.*;

import java.util.*;

public class LaneView implements ActionListener{

	private int roll;
	private boolean initDone = true;

	private JPanel jp;

	Vector bowlers;
	int cur, num;
	Iterator bowlIt;

	JPanel[][] balls, balls2;
	JLabel[][] ballLabel, ballLabel2;
	JPanel[][] scores, scores2;
	JLabel[][] scoreLabel, scoreLabel2;
	JPanel[][] ballGrid, ballGrid2;
	JPanel[] pins, pins2;
	JComboBox[] reactions;
	JButton[] button1, button2, button3;
	private int framesNow = 23;
	private boolean frame1_made = false;
	private boolean frame2_made = false;
	Lane lane;
	public void setFrames(int x){
		framesNow = x;
	}
	public LaneView(Lane lane, int laneNum) {

		this.lane = lane;
		initDone = true;
		jp = new JPanel();
	}

	public JPanel makeFrame(Party party) {

		initDone = false;
		bowlers = party.getMembers();
		int numBowlers = bowlers.size();
		num = numBowlers;
		JPanel panel = new JPanel();

		panel.setLayout(new GridLayout(0, 1));

		balls = new JPanel[numBowlers][23];
		ballLabel = new JLabel[numBowlers][23];
		scores = new JPanel[numBowlers][10];
		scoreLabel = new JLabel[numBowlers][10];
		ballGrid = new JPanel[numBowlers][10];
		pins = new JPanel[numBowlers];
		reactions = new JComboBox[numBowlers];
		button1 = new JButton[numBowlers];
		button2 = new JButton[numBowlers];
		button3 = new JButton[numBowlers];
		
		for (int i = 0; i != numBowlers; i++) {
			for (int j = 0; j != 23; j++) {
				ballLabel[i][j] = new JLabel(" ");
				balls[i][j] = new JPanel();
				balls[i][j].setBorder(
					BorderFactory.createLineBorder(Color.BLACK));
				balls[i][j].add(ballLabel[i][j]);
			}
		}

		for (int i = 0; i != numBowlers; i++) {
			for (int j = 0; j != 9; j++) {
				ballGrid[i][j] = new JPanel();
				ballGrid[i][j].setLayout(new GridLayout(0, 3));
				ballGrid[i][j].add(new JLabel("  "), BorderLayout.EAST);
				ballGrid[i][j].add(balls[i][2 * j], BorderLayout.EAST);
				ballGrid[i][j].add(balls[i][2 * j + 1], BorderLayout.EAST);
			}
			int j = 9;
			ballGrid[i][j] = new JPanel();
			ballGrid[i][j].setLayout(new GridLayout(0, 3));
			ballGrid[i][j].add(balls[i][2 * j]);
			ballGrid[i][j].add(balls[i][2 * j + 1]);
			ballGrid[i][j].add(balls[i][2 * j + 2]);
		}

		for (int i = 0; i != numBowlers; i++) {
			pins[i] = new JPanel();
			pins[i].setBorder(
				BorderFactory.createTitledBorder(
					((Bowler) bowlers.get(i)).getNick()));
			pins[i].setLayout(new GridLayout(0, 13));
			for (int k = 0; k != 10; k++) {
				scores[i][k] = new JPanel();
				scoreLabel[i][k] = new JLabel("  ", SwingConstants.CENTER);
				scores[i][k].setBorder(
					BorderFactory.createLineBorder(Color.BLACK));
				scores[i][k].setLayout(new GridLayout(0, 1));
				scores[i][k].add(ballGrid[i][k], BorderLayout.EAST);
				scores[i][k].add(scoreLabel[i][k], BorderLayout.SOUTH);
				pins[i].add(scores[i][k], BorderLayout.EAST);
			}
				
			ImageIcon haha = new ImageIcon("haha.png");
			ImageIcon wow = new ImageIcon("wow.png");
			ImageIcon happy = new ImageIcon("happy.png");
			int iconSize = 29;
			haha = new ImageIcon(haha.getImage().getScaledInstance(iconSize, iconSize, java.awt.Image.SCALE_SMOOTH));
			wow = new ImageIcon(wow.getImage().getScaledInstance(iconSize, iconSize, java.awt.Image.SCALE_SMOOTH));
			happy = new ImageIcon(happy.getImage().getScaledInstance(iconSize, iconSize, java.awt.Image.SCALE_SMOOTH));
			
			button1[i] = new JButton(haha);
			button1[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
			button2[i] = new JButton(wow);
			button2[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
			button3[i] = new JButton(happy);
			button3[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
			button1[i].addActionListener(this);
			button3[i].addActionListener(this);
			button2[i].addActionListener(this);
//				reactions[i].setLayout(new GridLayout(0, 1));
			pins[i].add(button1[i]);
			pins[i].add(button2[i]);
			pins[i].add(button3[i]);
			
			panel.add(pins[i]);
		}

		initDone = true;
		return panel;
	}
	
	public JPanel makeFrame2(Party party){
		initDone = false;
		bowlers = party.getMembers();
		int numBowlers = bowlers.size();
		JPanel panel = new JPanel();

		panel.setLayout(new GridLayout(0, 1));
		balls2 = new JPanel[numBowlers][8];
		ballLabel2 = new JLabel[numBowlers][8];
		scores2 = new JPanel[numBowlers][4];
		scoreLabel2 = new JLabel[numBowlers][4];
		ballGrid2 = new JPanel[numBowlers][4];
		pins2 = new JPanel[numBowlers];
		for (int i = 0; i != numBowlers; i++) {
			for (int j = 0; j != 8; j++) {
				ballLabel2[i][j] = new JLabel(" ");
				balls2[i][j] = new JPanel();
				balls2[i][j].setBorder(
					BorderFactory.createLineBorder(Color.BLACK));
				balls2[i][j].add(ballLabel2[i][j]);
			}
		}
		for (int i = 0; i != numBowlers; i++) {
			for (int j = 0; j != 4; j++) {
				ballGrid2[i][j] = new JPanel();
				ballGrid2[i][j].setLayout(new GridLayout(0, 2));
				ballGrid2[i][j].add(balls2[i][2 * j], BorderLayout.EAST);
				ballGrid2[i][j].add(balls2[i][2 * j + 1], BorderLayout.EAST);
			}
//			int j = 1;
//			ballGrid2[i][j] = new JPanel();
//			ballGrid2[i][j].setLayout(new GridLayout(0, 3));
//			ballGrid2[i][j].add(balls2[i][2 * j]);
//			ballGrid2[i][j].add(balls2[i][2 * j + 1]);
//			ballGrid2[i][j].add(balls2[i][2 * j + 1]);
		}
		for (int i = 0; i != numBowlers; i++) {
			pins2[i] = new JPanel();
			pins2[i].setBorder(
				BorderFactory.createTitledBorder(
					((Bowler) bowlers.get(i)).getNick()));
			pins2[i].setLayout(new GridLayout(0, 4));
			for (int k = 0; k != 4; k++) {
				scores2[i][k] = new JPanel();
				scoreLabel2[i][k] = new JLabel("  ", SwingConstants.CENTER);
				scores2[i][k].setBorder(
					BorderFactory.createLineBorder(Color.BLACK));
				scores2[i][k].setLayout(new GridLayout(0, 1));
				scores2[i][k].add(ballGrid2[i][k], BorderLayout.EAST);
				scores2[i][k].add(scoreLabel2[i][k], BorderLayout.SOUTH);
				pins2[i].add(scores2[i][k], BorderLayout.EAST);
			}
			panel.add(pins2[i]);
		}
		
		initDone = true;
		return panel;
	}
	
	private void addBallLabel(int k, int i, LaneEvent le){
		if (((int[]) ((HashMap) le.getScore())
				.get(bowlers.get(k)))[i]
				== -1) return;
		boolean check = i > 0 && ((int[]) ((HashMap) le.getScore())
				.get(bowlers.get(k)))[i]
				+ ((int[]) ((HashMap) le.getScore())
					.get(bowlers.get(k)))[i
				- 1]
				== 10
			&& i % 2 == 1;
		if (((int[]) ((HashMap) le.getScore())
				.get(bowlers.get(k)))[i]
				== 10
				&& (i % 2 == 0 || i == 19))
				ballLabel[k][i].setText("X");
			else if (check == true)
				ballLabel[k][i].setText("/");
			else if ( ((int[])((HashMap) le.getScore()).get(bowlers.get(k)))[i] == -2 ){
				
				ballLabel[k][i].setText("F");
			} else
				ballLabel[k][i].setText(
					(new Integer(((int[]) ((HashMap) le.getScore())
						.get(bowlers.get(k)))[i]))
						.toString());
	}
	
	private void addBallLabel2(int k, int i, LaneEvent le){
		if (((int[]) ((HashMap) le.getScore())
				.get(bowlers.get(k)))[i]
				== -1) {
			return;
		}
		boolean check = i > 0 && ((int[]) ((HashMap) le.getScore())
				.get(bowlers.get(k)))[i]
				+ ((int[]) ((HashMap) le.getScore())
					.get(bowlers.get(k)))[i
				- 1]
				== 10
			&& i % 2 == 1;
		if (((int[]) ((HashMap) le.getScore())
				.get(bowlers.get(k)))[i]
				== 10){
				ballLabel2[k][i].setText("X");
		}
		else if (check == true)
			ballLabel2[k][i].setText("/");	
		else if ( ((int[])((HashMap) le.getScore()).get(bowlers.get(k)))[i] == -2 ){
				
				ballLabel2[k][i].setText("F");
		}
		else if(((int[]) ((HashMap) le.getScore())
					.get(bowlers.get(k)))[i] != -1){
			ballLabel2[k][i].setText(
				(new Integer(((int[]) ((HashMap) le.getScore())
					.get(bowlers.get(k)))[i]))
					.toString());
		}
	}
	
	private boolean canMakeFrame(LaneEvent le){
		return (le.getFrameNum() == 1 || le.getFrameNum() == 100) 
				&& le.getBall() == 0
				&& le.getIndex() == 0;
	}
	public void receiveLaneEvent(LaneEvent le) {
		if(le.getExtended() == true) {
			framesNow = 1;
			
		}
		
		if (lane.isPartyAssigned()) {
		
			int numBowlers = le.getParty().getMembers().size();
		
			while (!initDone) {
				System.out.println("chillin' here.");
				try {
					Thread.sleep(1);
				} catch (Exception e) {
				}
			}
			
			if (canMakeFrame(le)) {
				
				System.out.println("Making the frame.");
				System.out.println(framesNow);
				if(framesNow == 23) {
					if(!frame1_made) {
						jp = makeFrame(le.getParty());
						frame1_made = true;
					}
				}
				else {
					if(!frame2_made) {
						jp = makeFrame2(le.getParty());
						frame2_made = true;
					}
				}
			}
		
			int[][] lescores = le.getCumulScore();
			for (int k = 0; k < numBowlers; k++) {
				for (int i = 0; i <= le.getFrameNum() - 1; i++) {
					if (lescores[k][i] != 0){
						if(framesNow != 1){
							scoreLabel[k][i].setText(
									(new Integer(lescores[k][i])).toString());
						}
						else{
							scoreLabel2[k][i].setText(
									(new Integer(lescores[k][i])).toString());
						}
					}
//					if(lescores[k][i] == -31){
//						if(framesNow != 1){
//							scoreLabel[k][i].setText(
//									(new Integer(0)).toString());
//						}
//						else{
//							scoreLabel2[k][i].setText(
//									(new Integer(0)).toString());
//						}
//					}
						
				}
				
				for (int i = 0; i < (framesNow>21?21:framesNow+7); i++) {
					if(framesNow == 23) addBallLabel(k, i, le);
					else {
						addBallLabel2(k, i, le);
					}
				}
			}
		}
	}

	public JPanel showLane() {
		return jp;
	}
	
	public void playSound(String soundName)
	 {
	   try 
	   {
         AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File(soundName));
         AudioFormat format = inputStream.getFormat();
         DataLine.Info info = new DataLine.Info(Clip.class, format);
         Clip clip = (Clip)AudioSystem.getLine(info);
         clip.open(inputStream);
         clip.start();
	   }
	   catch(Exception ex)
	   {
	     System.out.println("Error with playing sound.");
	     ex.printStackTrace( );
	   }
	 }
	
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		for(int i=0;i<bowlers.size();i++){
			if(e.getSource().equals(button1[i])){
				playSound("haha.wav");
				System.out.println(((Bowler)(bowlers.get(i))).getNick() + " reacted " +
						"Noob stuff :p");
			}
			if(e.getSource().equals(button2[i])){
				playSound("wow.wav");
				System.out.println(((Bowler)(bowlers.get(i))).getNick() + " reacted " +
						"Nice shot :o");
			}
			if(e.getSource().equals(button3[i])){
				playSound("happy.wav");
				System.out.println(((Bowler)(bowlers.get(i))).getNick() + " reacted " +
						"Lucky shot :D");
			}
		}
	}
}
