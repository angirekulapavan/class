import java.util.HashMap;
import java.util.Vector;


public class ExtendedGame{
	private Pinsetter setter = new Pinsetter();
	
	public Bowler checkScores(Lane lane, Party party, int[][] cumulScores){
		HashMap scores = lane.getScores();
		int max1=-1, max2=-1, index1 = 0, index2 = 0;
		for(int i=0;i<party.getMembers().size();i++){
			if(cumulScores[i][9] > max1){
				max2 = max1;index2 = index1;
				max1 = cumulScores[i][9];index1 = i;
			}
			else if(cumulScores[i][9] > max2){
				max2 = cumulScores[i][9];index2 = i;
			}
		}	
		if(max2 == -1) return new Bowler(null, null, null);
		Vector extendedBowlers = new Vector();
		extendedBowlers.add(party.getMembers().get(index1));
		extendedBowlers.add(party.getMembers().get(index2));
		Party extendedMatch = new Party(extendedBowlers);
		Party tempParty = new Party(party.getMembers());
		party = extendedMatch;
		
//		System.out.println(.isPartyAssigned());
//		LaneView lv = new LaneView(lane, lane.getLaneNumber());
//		LaneEvent le = lane.lanePublish();
//		le.setParty(party);
		int[][] cumulScore = new int[2][4];
		lane.setParty(party);
//		HashMap score = new HashMap();
//		int[] toPut = new int[25];
//		for ( int i = 0; i != 25; i++){
//			toPut[i] = -1;
//		}
//		score.put(party.getMembers().get(0), toPut);
//		toPut = new int[25];
//		for ( int i = 0; i != 25; i++){
//			toPut[i] = -1;
//		}
//		score.put(party.getMembers().get(1), toPut);
//		le.setCumulScore(cumulScore);
//		le.setFrameNum(1);
//		System.out.println(le.getFrameNum());
//		le.setIndex(0);
//		le.setBall(0);
//		
//		le.setScore(score);
//		lv.setFrames(1);
//		lv.receiveLaneEvent(le);
//		
//		lv.show();
		lane.setCumulScore(cumulScore);
		Lane afterMatch = extendedPlay(lane, party, cumulScores, max1, max2);
		return declareWinner(cumulScores, afterMatch, index1, index2, scores, party);
	}
	
	private Lane extendedPlay(Lane lane, Party party, int[][] cumulScores, int max1, int max2){
		lane.resetBowlerIterator();
		lane.increamentBowler();
		int[][] temp = lane.getCumulScore();
		temp[0][0] = 0;
		lane.setCumulScore(temp);
		lane.setFrameNum(0);
		lane.resetScores();
		lane.setEndGame(true);
		lane.run2(max1, max2);
		return lane;
	}
	
	private Bowler declareWinner(int[][] before1, Lane after, int index1, int index2, HashMap scores, Party party){
		if(after.getFrameNumber() < 2) return (Bowler) party.getMembers().get(0);
		int[][] finalScores1 = before1;
		int[][] extendedScores = after.getCumulScore();
		System.out.println("P1 " + finalScores1[index1][9] + " " + extendedScores[0][3]);
		System.out.println("P2 " + finalScores1[index2][9] + " " + extendedScores[1][3]);
		System.out.println("extra " + extendedScores[1][0]);
		if(finalScores1[index1][9] < finalScores1[index2][9] + extendedScores[1][0]){
			
			if(finalScores1[index1][9] + extendedScores[0][3] < finalScores1[index2][9] + extendedScores[1][3]){
				return (Bowler) party.getMembers().get(1);
				// declare player 2 as winner;
			}else if(finalScores1[index1][9] + extendedScores[0][3] > finalScores1[index2][9] + extendedScores[1][3]){
				return (Bowler) party.getMembers().get(0);
				// declare player 1 as winner;
			}else{
				HashMap hm = scores;
				int[] p1 = (int[]) hm.get(party.getMembers().get(0));
				int[] p2 = (int[]) hm.get(party.getMembers().get(1));
				int strike1 = 0, strike2 = 0;
				for(int i=0;i<21;i++){
					if(p1[i] == 10) strike1++;
					if(p2[i] == 10) strike2++;
				}
				if(strike1 >= strike2) {
					return (Bowler) party.getMembers().get(0);
					// declare p1 as winner;
				}else {
					return (Bowler) party.getMembers().get(1);
					// declare p2 as winner;
				}
			}
		}else{
			System.out.println("Nothing has changed");
			return (Bowler) party.getMembers().get(0);
			// return player1 as winner;
		}
	}
}
